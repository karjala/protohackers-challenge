#!/usr/bin/env perl

use v5.36;
use warnings;
use FindBin '$RealBin';
use lib "$RealBin/../local/lib/perl5", "$RealBin/../lib";

use Mojo::IOLoop;
use Mojo::IOLoop::Server;
use My::Mojo::IOLoop::Stream;

use List::Util 'sum0';

# Create listen socket
my $server = Mojo::IOLoop::Server->new;

my %streams;
$server->on(accept => sub ($_server, $handle) {
    my $stream = My::Mojo::IOLoop::Stream->new($handle);

    $stream->timeout(0);

    my ($buffer, @timeline) = '';
    $stream->on(read => sub ($_stream, $bytes) {
        $buffer .= $bytes;
        while ($buffer =~ s/^(.)(.{4})(.{4})//s) {
            my ($type, $arg1, $arg2) = ($1, map { unpack('N!', $_) } ($2, $3));
            say "$type $arg1 $arg2";
            if ($type eq 'I') {
                push @timeline, { time => $arg1, price => $arg2 };
            } elsif ($type eq 'Q') {
                my $avg = do {
                    if ($arg1 > $arg2) { 0 }
                    else {
                        my @selection = grep { $arg1 <= $_->{time} <= $arg2 } @timeline;
                        if (! @selection) { 0 }
                        else {
                            my $sum = sum0(map $_->{price}, @selection);
                            int($sum / @selection);
                        }
                    }
                };
                $_stream->write(pack('N!', $avg));
                say $avg;
            }
        }
    });

    $stream->on(close => sub ($_stream) {
        delete $streams{$_stream};
    });

    $streams{$stream} = $stream;

    $stream->start;
});
$server->listen(port => 2999);

# Start accepting connections
$server->start;

# Start reactor if necessary
$server->reactor->start unless $server->reactor->is_running;
