#!/usr/bin/env perl

use v5.36;
use FindBin '$RealBin';
use lib "$RealBin/../local/lib/perl5", "$RealBin/../lib";

use Mojo::IOLoop::Server;
use My::Mojo::IOLoop::Stream;
use Mojo::JSON 'encode_json', 'decode_json', 'true', 'false';

my $server = Mojo::IOLoop::Server->new;

my %streams;
$server->on(accept => sub ($_server, $handle) {
    my $stream = My::Mojo::IOLoop::Stream->new($handle);

    my $input_buffer = '';
    $stream->on(read => sub ($_stream, $bytes) {
        $input_buffer .= $bytes;
        my @lines_to_process = $input_buffer =~ /^(.*?\n)/mg;
        $input_buffer =~ s/^.*\n//s;
        LINE:
        foreach my $line (@lines_to_process) {
            print $line;
            my $number = eval {
                my $obj = decode_json $line;
                ($obj->{method} // '') eq 'isPrime' or die;
                my $number = $obj->{number};
                length $number
                    and !ref $number
                    and encode_json($number) =~ /^\-?[0-9]/
                    or die;
                $number;
            };
            if ($@) {
                $_stream->write("NOT OK\n");
                $_stream->close_gracefully;
                last LINE;
            } else {
                if ($number =~ /^[0-9]+\z/ and $number >= 2) {
                    my $sqrt = sqrt $number;
                    for (my $i = 2; $i <= $sqrt + 0.001; $i++) {
                        if ($number % $i == 0) {
                            $_stream->write(encode_json({
                                method => 'isPrime',
                                prime  => false,
                            }) . "\n");
                            next LINE;
                        }
                    }
                    $_stream->write(encode_json({
                        method => 'isPrime',
                        prime  => true,
                    }) . "\n");
                    next LINE;
                } else {
                    $_stream->write(encode_json({
                        method => 'isPrime',
                        prime  => false,
                    }) . "\n");
                    next LINE;
                }
            }
        }
    });

    $streams{$stream} = $stream;

    $stream->on(close => sub ($_stream) {
        delete $streams{$_stream};
    });

    $stream->start;
});
$server->listen(port => 2999);

# Start accepting connections
$server->start;

# Start reactor if necessary
$server->reactor->start unless $server->reactor->is_running;
