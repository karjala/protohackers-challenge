package My::Mojo::IOLoop::Stream;

use Mojo::Base 'Mojo::IOLoop::Stream';

use Errno qw(EAGAIN ECONNRESET EINTR EWOULDBLOCK);

sub _read {
    my $self = shift;

    if (defined(my $read = $self->{handle}->sysread(my $buffer, 131072, 0))) {
        $self->{read} += $read;
        return $read == 0 ? $self->close_gracefully : $self->emit(read => $buffer)->_again;
    }

    # Retry
    return undef if $! == EAGAIN || $! == EINTR || $! == EWOULDBLOCK;

    # Closed (maybe real error)
    $! == ECONNRESET ? $self->close : $self->emit(error => $!)->close;
}

1;
