#!/usr/bin/env perl

use v5.36;
use FindBin '$RealBin';
use lib "$RealBin/../local/lib/perl5", "$RealBin/../lib";

use Mojo::IOLoop::Server;
use My::Mojo::IOLoop::Stream;

# Create listen socket
my $server = Mojo::IOLoop::Server->new;

my %users;
$server->on(accept => sub ($_server, $handle) {
    my $stream = My::Mojo::IOLoop::Stream->new($handle);

    # phase 1 = pre-login, phase 2 = logged in
    my $user = { username => undef, stream => $stream };
    my $buffer = '';
    $stream->on(read => sub ($_stream, $bytes) {
        $buffer .= $bytes;
        LINE:
        while ($buffer =~ s/^(.*?)\n//) {
            my $line = $1;
            $line =~ s/\s+\z//;

            if (! defined $user->{username}) {
                $line =~ /^[a-z0-9]{1,16}\z/i or do {
                    $_stream->write("illegal username\n");
                    $_stream->close_gracefully;
                    return;
                };
                my $username = $user->{username} = $line;

                # announce presence to other users
                my @other_users = grep $_ ne $user, grep defined($_->{username}), values %users;
                foreach my $other_user (@other_users) {
                    $other_user->{stream}->write("* $username has entered the room\n");
                }

                $_stream->write(
                    "* The room contains: " . join(', ', map $_->{username}, @other_users) . "\n"
                );
            } else {
                length $line <= 1_000 or next LINE;

                my @other_users = grep $_ ne $user, grep defined($_->{username}), values %users;
                my $full_msg = "[$user->{username}] $line\n";
                foreach my $other_user (@other_users) {
                    $other_user->{stream}->write($full_msg);
                }
            }
        }
    });
    $stream->on(close => sub ($_stream) {
        if (defined $user->{username}) {
            my @other_users = grep $_ ne $user, grep defined($_->{username}), values %users;
            my $full_msg = "* $user->{username} has left the room\n";
            $_->{stream}->write($full_msg) foreach @other_users;
        }
        delete $users{$user};
    });

    $users{$user} = $user;

    $stream->start;

    $stream->write("Welcome to budgetchat! What shall I call you?\n");
});
$server->listen(port => 2999);

# Start accepting connections
$server->start;

# Start reactor if necessary
$server->reactor->start unless $server->reactor->is_running;
