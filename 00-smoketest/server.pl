#!/usr/bin/env perl

use v5.36;
use warnings;
use FindBin '$RealBin';
use lib "$RealBin/../local/lib/perl5", "$RealBin/../lib";

use Mojo::IOLoop;
use Mojo::IOLoop::Server;
use My::Mojo::IOLoop::Stream;

# Create listen socket
my $server = Mojo::IOLoop::Server->new;

my %streams;
$server->on(accept => sub ($_server, $handle) {
    my $stream = My::Mojo::IOLoop::Stream->new($handle);

    $stream->on(read => sub ($_stream, $bytes) {
        $_stream->write($bytes);
    });

    $stream->on(close => sub ($_stream) {
        delete $streams{$_stream};
    });

    $streams{$stream} = $stream;

    $stream->start;
});
$server->listen(port => 2999);

# Start accepting connections
$server->start;

# Start reactor if necessary
$server->reactor->start unless $server->reactor->is_running;
