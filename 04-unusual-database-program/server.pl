#!/usr/bin/env perl

use v5.36;
use warnings;
use FindBin '$RealBin';
use lib "$RealBin/../local/lib/perl5", "$RealBin/../lib";

use My::Mojo::IOLoop::UDPServer;

STDOUT->autoflush(1);

my $server = My::Mojo::IOLoop::UDPServer->new;

my %database;

$server->on(message => sub ($_server, $msg) {
    if ($msg =~ /^(.*?)=(.*)\z/s) {
        my ($key, $value) = ($1, $2);
        $database{$key} = $value;
    } else {
        my $key = $msg;
        if ($key eq 'version') {
            $_server->write('version=Alex server 0.1');
        } else {
            my $value = $database{$key} // '';
            $_server->write("$key=$value");
        }
    }
});

$server->listen({ port => 2999 });

$server->start;

Mojo::IOLoop->start;