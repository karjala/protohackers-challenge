#!/usr/bin/env perl

use v5.38;
use FindBin '$RealBin';
use lib "$RealBin/../local/lib/perl5", "$RealBin/../lib";

use My::Mojo::IOLoop::Stream;
use Mojo::IOLoop::Server;
use Mojo::IOLoop;
use List::AllUtils qw/min max min_by max_by any/;
use Data::Dumper;
use POSIX 'floor';

no autovivification;
use experimental 'class';

# Create listen socket
my $server = Mojo::IOLoop::Server->new;

my %streams;
my %car_locations; # plate -> road -> {mile, timestamp}
my %sent_tickets; # plate -> day
my %dispatchers; # $what_is_client -> { what_is_client, stream }
my %outboxes; # road -> [tickets]
$server->on(accept => sub ($_server, $handle) {
    my $stream = My::Mojo::IOLoop::Stream->new($handle);
    $stream->timeout(0);

    my ($has_heartbeat, $has_identified);
    my $what_is_client;

    my $buffer = Buffer->new;
    $stream->on(read => sub ($_stream, $bytes) {
        $buffer->append_to_buffer($bytes);
        $buffer->reset_cursor;
        MESSAGE:
        while (1) {
            my $msg = $buffer->read_message // last MESSAGE;
            print Dumper $msg;
            if ($msg eq 'error') {
                $_stream->write("\x{10}\x{03}bad");
                $_stream->close_gracefully;
                last MESSAGE;
            } elsif ($msg->{type} eq 'WantHeartbeat') {
                if ($has_heartbeat++) {
                    $_stream->write("\x{10}\x{03}bad");
                    $_stream->close_gracefully;
                    last MESSAGE;
                }
                my $interval = $msg->{interval};
                if ($interval == 0) { next MESSAGE }
                my $id = Mojo::IOLoop->recurring($interval / 10, sub {
                    $_stream->write("\x{41}");
                });
                $_stream->on(close => sub { Mojo::IOLoop->remove($id) });
            } elsif ($msg->{type} eq 'IAmCamera') {
                if ($has_identified++) {
                    $_stream->write("\x{10}\x{03}bad");
                    $_stream->close_gracefully;
                    last MESSAGE;
                }
                $what_is_client = $msg;
            } elsif ($msg->{type} eq 'Plate') {
                if (!eqq(scalar(eval { $what_is_client->{type} }), 'IAmCamera')) {
                    $_stream->write("\x{10}\x{03}bad");
                    $_stream->close_gracefully;
                    last MESSAGE;
                }
                my ($plate, $timestamp) = $msg->@{qw/ plate timestamp /};
                my ($road, $mile, $limit) = $what_is_client->@{qw/ road mile limit /};

                # store location
                push $car_locations{$plate}{$road}->@*, { mile => $mile, timestamp => $timestamp };

                # has limit been exceeded?
                my $just_after = min_by {$_->{timestamp}} grep $_->{timestamp} > $timestamp, $car_locations{$plate}{$road}->@*;
                my $just_before = max_by {$_->{timestamp}} grep $_->{timestamp} < $timestamp, $car_locations{$plate}{$road}->@*;
                INSTANCE:
                foreach my $instance (grep defined, $just_before, $just_after) {
                    my $distance = abs($instance->{mile} - $mile);
                    my $min_timestamp = min($instance->{timestamp}, $timestamp);
                    my $max_timestamp = max($instance->{timestamp}, $timestamp);
                    my $avg_speed = 3_600 * $distance / ($max_timestamp - $min_timestamp);
                    if ($avg_speed > $limit + 0.49) {
                        my $min_day = floor($min_timestamp / 86_400);
                        my $max_day = floor($max_timestamp / 86_400);

                        # check if ticket has been sent for any of those days
                        foreach my $day ($min_day .. $max_day) {
                            if (exists $sent_tickets{$plate}{$day}) {
                                next INSTANCE;
                            }
                        }

                        # TODO: send ticket
                        $sent_tickets{$plate}->@{$min_day .. $max_day} = ();
                        my $mile1 = $timestamp == $min_timestamp ? $mile : $instance->{mile};
                        my $mile2 = $timestamp == $max_timestamp ? $mile : $instance->{mile};
                        my $ts1 = $timestamp == $min_timestamp ? $timestamp : $instance->{timestamp};
                        my $ts2 = $timestamp == $max_timestamp ? $timestamp : $instance->{timestamp};

                        push $outboxes{$road}->@*, {
                            plate      => $plate,
                            road       => $road,
                            mile1      => $mile1,
                            timestamp1 => $ts1,
                            mile2      => $mile2,
                            timestamp2 => $ts2,
                            speed      => int($avg_speed * 100),
                        };
                        process_outbox($road);
                        say 'send ticket';
                    }
                }
            } elsif ($msg->{type} eq 'IAmDispatcher') {
                if ($has_identified++) {
                    $_stream->write("\x{10}\x{03}bad");
                    $_stream->close_gracefully;
                    last MESSAGE;
                }
                $what_is_client = $msg;
                $dispatchers{$what_is_client} = {
                    what_is_client => $what_is_client,
                    stream         => $_stream,
                };
                process_outbox($_, $_stream) foreach $what_is_client->{roads}->@*;
                $_stream->on(close => sub {
                    delete $dispatchers{$what_is_client};
                });
            }
        }
    });

    $stream->on(close => sub ($_stream) {
        delete $streams{$stream};
    });

    $streams{$stream} = $stream;
    $stream->start;
});

$server->listen(port => 2999);

# Start accepting connections
$server->start;

# Start reactor if necessary
$server->reactor->start unless $server->reactor->is_running;

class Buffer {
    field $buffer = '';
    field $cursor = 0;

    method append_to_buffer ($bytes) {
        $buffer .= $bytes;
    }

    method reset_cursor { $cursor = 0 }

    method read_message {
        my $type_num = $self->read_u8 // return undef;
        if ($type_num == 0x40) {
            my $interval = $self->read_u32 // return undef;
            $self->chop;
            return {
                type     => 'WantHeartbeat',
                interval => $interval,
            };
        } elsif ($type_num == 0x80) {
            my $road = $self->read_u16 // return undef;
            my $mile = $self->read_u16 // return undef;
            my $limit = $self->read_u16 // return undef;
            $self->chop;
            return {
                type  => 'IAmCamera',
                road  => $road,
                mile  => $mile,
                limit => $limit,
            };
        } elsif ($type_num == 0x20) {
            my $plate = $self->read_str // return undef;
            my $timestamp = $self->read_u32 // return undef;
            $self->chop;
            return {
                type      => 'Plate',
                plate     => $plate,
                timestamp => $timestamp,
            };
        } elsif ($type_num == 0x81) {
            my $num_roads = $self->read_u8 // return undef;
            my @roads;
            for (my $i = 1; $i <= $num_roads; $i++) {
                my $road = $self->read_u16 // return undef;
                push @roads, $road;
            }
            $self->chop;
            return {
                type  => 'IAmDispatcher',
                roads => \@roads,
            };
        } else {
            return 'error';
        }
    }

    method read_chars ($length) {
        length $buffer >= $cursor + $length or return undef;
        my $substr = substr $buffer, $cursor, $length;
        $cursor += $length;
        return $substr;
    }

    method read_u8 {
        my $chars = $self->read_chars(1) // return undef;
        return unpack 'C', $chars;
    }

    method read_u16 {
        my $chars = $self->read_chars(2) // return undef;
        return unpack 'S>', $chars;
    }

    method read_u32 {
        my $chars = $self->read_chars(4) // return undef;
        return unpack 'L>', $chars;
    }

    method read_str {
        my $string_length = $self->read_u8 // return undef;
        my $string = $self->read_chars($string_length) // return undef;
        return $string;
    }

    method chop {
        substr($buffer, 0, $cursor) = '';
        $cursor = 0;
    }
}

sub eqq ($x, $y) {
    defined $x or return !defined $y;
    defined $y or return !!0;
    return $x eq $y;
}

sub process_outbox ($road, $stream = undef) {
    $outboxes{$road} and $outboxes{$road}->@* or return;
    my @dispatchers; @dispatchers = grep {
        my $what_is_client = $_->{what_is_client};
        any {$_ eq $road} $what_is_client->{roads}->@*;
    } values %dispatchers or return unless $stream;
    while (my $ticket = shift $outboxes{$road}->@*) {
        my $_stream = $stream // $dispatchers[rand @dispatchers]{stream};
        $_stream->write(
            "\x{21}" . # Ticket
                chr(length $ticket->{plate}) .
                $ticket->{plate} .
                pack('S>', $ticket->{road}) .
                pack('S>', $ticket->{mile1}) .
                pack('L>', $ticket->{timestamp1}) .
                pack('S>', $ticket->{mile2}) .
                pack('L>', $ticket->{timestamp2}) .
                pack('S>', $ticket->{speed})
        );
    }
}
