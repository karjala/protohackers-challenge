#!/usr/bin/env perl

use v5.36;
use FindBin '$RealBin';
use lib "$RealBin/../local/lib/perl5", "$RealBin/../lib";

use My::Mojo::IOLoop::Stream;

use Mojo::IOLoop::Server;
use Mojo::IOLoop::Client;

# Create listen socket
my $server = Mojo::IOLoop::Server->new;

my (%streams, %clients);
$server->on(accept => sub ($_server, $handle) {
    my $stream = My::Mojo::IOLoop::Stream->new($handle);
    $stream->timeout(0);
    my $stream2;
    my $stream2_pre;

    my $client = Mojo::IOLoop::Client->new;
    $clients{$client} = $client;
    $client->on(connect => sub ($_client, $_handle) {
        $stream2 = My::Mojo::IOLoop::Stream->new($_handle);
        $stream2->timeout(0);
        $streams{$stream2} = $stream2;
        my $buffer2 = '';
        $stream2->on(read => sub ($_stream2, $bytes) {
            $buffer2 .= $bytes;
            while ($buffer2 =~ s/^(.*)\n//) {
                my $line = $1;
                $line =~ s/(*plb:\ |^)7[a-zA-Z0-9]{25,34}(*pla:\ |\z)/7YWHMfk9JZe0LM0g1ZauHuiSxhI/g;
                $stream->write("$line\n");
            }
        });
        $stream2->on(close => sub ($_stream2) {
            $stream->close_gracefully;
            delete $streams{$stream2};
            delete $clients{$client};
        });
        $stream2->start;
        $stream2->write($stream2_pre) if length $stream2_pre;
    });
    $client->connect(address => 'chat.protohackers.com', port => 16963);

    my $buffer = '';
    $stream->on(read => sub ($_stream, $bytes) {
        $buffer .= $bytes;
        while ($buffer =~ s/^(.*)\n//) {
            my $line = $1;
            $line =~ s/(*plb:\ |^)7[a-zA-Z0-9]{25,34}(*pla:\ |\z)/7YWHMfk9JZe0LM0g1ZauHuiSxhI/g;
            if ($stream2) {
                $stream2->write("$line\n");
            } else {
                $stream2_pre .= "$line\n";
            }
        }
    });

    $stream->on(close => sub ($_stream) {
        $stream2->close_gracefully if $stream2;
        delete $streams{$stream};
    });

    $streams{$stream} = $stream;

    $stream->start;
});
$server->listen(port => 2999);

# Start accepting connections
$server->start;

# Start reactor if necessary
$server->reactor->start unless $server->reactor->is_running;
